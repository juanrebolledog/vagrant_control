#!/usr/bin/env bash

VM_DIR=~/workspace/vms

if [ -z "$1" ]; then
    echo "VM name required"
fi

if [ -z "$2" ]; then
    echo "Command required"
fi

if [ -d $VM_DIR/$1 ]; then
    if [ -f $VM_DIR/$1/Vagrantfile ]; then
        cd $VM_DIR/$1
        vagrant $2
    fi
fi
